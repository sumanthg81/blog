<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ListController extends Controller
{
    public function show()
    {
        $characters = [
            'person 1' => 'real identity 1',
            'person 2' => 'real identity 2',
            'person 3' => 'real identity 3',
            'person 4' => 'real identity 4',
            'person 5' => 'real identity 5',
            'person 6' => 'real identity 6',
            'person 7' => 'real identity 7',

        ];

        return view('welcome')->withCharacters($characters);
    }
}
